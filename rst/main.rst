.. raw:: html

   <?php # 2 __DATE__ Bankvesenet
      $attrib_AC=0;
      $ATTRIB_link = 'normal';
      require_once 'full-graph.php';
      function echo_text($al) {
   ?>
   <div style="padding: 20px;">

===========
Bankvesenet
===========

*Av: |author| / |datetime|*

.. include:: revision-header.rst

Bankvesenet er en vesentlig del av samfunnet og det økonomiske systemet
siden det er her penger skapes.
Det er ikke helt enkelt å forstå hvordan bankvesenet fungerer.
Nå for tiden er det flere og flere som forkaster tidligere forklaringsmodeller
der man sier at banksystemets funksjon er å videreformidle innskudd fra sparere
til utlån til bedrifter og husholdninger.
I boken "Makroøkonomi" av Steinar Holden er det den moderne forklaringen som er brukt, i 11.2.3 heter det:
"Bankene kan rett og slett skape penger, noe som skjer når de låner ut
penger til husholdninger og bedrifter."

Den gammeldagse måten har vi eksempelvis i boken "Penger"
av Erling Røed Larsen:
Her starter spillet med at noen setter inn penger i banken som banken så låner ut
igjen mesteparten av, som så fører til et nytt innskudd som banken igjen låner ut
mesteparten av osv.
Den beskrivelsen medfører at det er umulig å si hvor pengene er:
De samme pengene befinner seg flere steder på én gang.

Den moderne beskrivelsen gjenåpner spørsmålet om hvor pengene *er*:
Ettersom banken skaper de pengene som du låner idét du låner dem,
så kan man hevde at pengene *er* på den kontoen som bankutskriften viser at de er,
for banken bruker jo ikke innskudd fra noen andre til å opprette nye penger.

Det er også et annet forhold som er interessant når det gjelder penger og gjeld.
Det føres statistikk over hvor mye penger banksektoren har skapt, samt hvor mye gjeld det finnes.
Til hjelp for å holde oversikten finnes det noen makrovariabler eller indikatorer.
Pengemengden heter M3, (tidligere M2) og mengden gjeld heter K2. Statistikkene føres av ssb.no.
Det som er interessant, er at M3 og K2 ikke nødvendigvis er like store.
Verdipapirer i form av obligasjoner kan påvirke M3 uten at K2 blir endret.

En kort sammenfatning av transaksjoner som påvirker makrovariablene
-------------------------------------------------------------------

Samfunnet deles inn i flere forskjellige økonomiske sektorer som har forskjellige egenskaper og oppgaver.
En grovinndeling som kan gi en forenklet framstilling av hvordan systemet fungerer er å dele i tre sektorer.
Vi har **pengeholdende sektor** der husholdninger, bedrifter,
kommuneforvaltningen og ideelle organisasjoner befinner seg.
Den andre sektoren består egentlig av flere: **pengeutstedende og pengenøytrale sektorer**. Her finner vi
sentralbanken og bankene, statsforvaltningen og utlandet. Jeg kaller denne sammenslåingen for
*pengeskapende sektor* nedenfor.
Den tredje er kreditt-sektoren, der enheter av typen finansieringsselskaper, kredittforetak, og forsikringsselskaper
befinner seg.
Kreditt-sektoren har noen likheter med enheter i pengeskapende sektor, men *den er en undersektor av pengeholdende sektor*.

M3 og K2 kan endre seg dersom verdipapirer, varer eller tjenester blir omsatt,
'kjøpt' eller 'solgt', mellom organisasjoner
som befinner seg i hver sin sektor, men ikke dersom de befinner seg i samme sektor.
Som verdipapir kan vi regne aksjer, obligasjoner og lånekontrakter.

Varer og tjenester
^^^^^^^^^^^^^^^^^^

Når disse blir omsatt i pengeholdende sektor, så endrer ikke K2 eller M3 seg.
Dette gjelder også dersom partene som utfører transaksjonen har forskjellige bankforbindelser.
I så fall vil det skje endel transaksjoner mellom de forskjellige bankene som konsekvens av
transaksjonen, men dette påvirker ikke M3 eller K2.
Dersom transaksjonen skjer mellom en enhet i pengeholdende sektor og utlandet, så påvirker dette M3.
M3 øker dersom noe blir solgt til utlandet, minker dersom noe blir kjøpt.

Aksjer
^^^^^^

Aksjer blir som regel omsatt mellom organisasjoner i pengeholdende sektor, og medfører dermed ikke
(som regel) endringer i K2 eller M3.

Obligasjoner
^^^^^^^^^^^^

Obligasjoner blir ofte omsatt mellom organisasjoner i forskjellige sektorer:
Hvis banksektoren kjøper en obligasjon av en bedrift, øker pengemengden, hvis banksektoren selger,
minker pengemengden (M3). K2 endres derimot ikke av slike transaksjoner.
Endringer i verdipapir-beholdningene framgår av balansene til bankene og sentralbanken,
men noen tilsvarende makroøkonomisk indikator som M3 og K2 finnes ikke.

Lån
^^^

Lånekontrakter som blir inngått mellom en part i pengeholdene sektor og en bank,
*blir* registrert i begge indikatorene (M3 og K2).
Når et huslån blir opprettet, øker både M3 og K2 med lånebeløpet.
Når lånet blir nedbetalt helt eller delvis, minker M3 og K2 tilsvarende størrelsen på avdraget.
Dersom lånekontrakten blir inngått mellom en part i pengeholdende sektor og kredittsektoren,
øker K2 mens M3 ikke endres, siden kredittsektoren ikke kan skape penger.

.. raw:: html

   <?php full_graph('makro.csv', 0, 'out', 2017); ?>

I grafen over er de mest sentrale makroøkonomiske indikatorene vist.
Grafen er satt opp slik at alt er målt i forhold til hver persons andel av brutto-nasjonalproduktet.
Av den grunn vil ikke økonomisk vekst vises.
Mer om dette senere.
Vi ser at pengemengden (M3) er omtrent halvparten av BNP i normal-år,
men avviker ganske mye fra dette i unormale år med krig eller store økonomiske problemer.
Vi ser også at lønnen er avtakende. Det kan tenkes å være to hovedgrunner til dette:
Etterhvert som større andel av befolkningen er i arbeid, blir det mindre lønn
(i forhold til hver innbyggers andel av BNP) å fordele til hver arbeidtaker.
Forholdet mellom lønns-utbetalinger og uttak av profitt i bedriftene vil også trekke
lønnen nedover dersom profitt-andelen øker.
En måte å vise økonomisk vekst i et slikt diagram-system er å bruke konsumpris-indeksen.
Hvis denne angis i forhold til BNP, vil kurven falle med tiden dersom vi har økonomisk vekst.
Dette skyldes at når varene blir billigere, så får man lønnen til å strekke lenger,
og dermed får man råd til å kjøpe mer.
Det at man har råd til å kjøpe mer er jo er en konsekvens av økonomisk vekst.

.. raw:: html

   <?php full_graph('priser-spenn.csv', 1, 'out', 2017); ?>

Her vises hvordan varene blir billigere med tiden dersom vi har økonomisk vekst.

Tilbake til den første grafen: Indikatoren med størst utslag er K2.
Gjelden har i de seneste årene gått kraftig opp både sett i forhold til BNP og den
generelle lønnsutviklingen.
Dette kan forklares enten med et bevisst valg om å inndra deler av pengemengden vha.
verdipapir-salg til pengeholdene sektor
eller at hele differansen skyldes låneopptak fra kredittsektoren, eller en kombinasjon.

Siden kredittsektoren ikke kan skape penger, må denne sektoren tiltrekke seg penger
som allerede finnes og videreformidle disse til andre personer eller enheter i
pengeholdende sektor som ønsker seg penger.
Altså kan K2 deles opp i delsummer som forteller hvilke sektorer opphavet til K2 stammer fra.
SSB har slike statistikker.
Tabell 06718 gir en bl. annet denne inndelingen.

.. raw:: html

   <?php full_graph('makro-kilder.csv', 1, 'out', 2017); ?>

I grafen over er det pengemengden som er satt som basis (100%) i stedet for BNP.
Denne viser igjen hvordan lønn i forhold til gjeld har blir mindre gunstig med tiden.
Kreditten kan som sagt deles opp i kreditt fra pengeskapende sektor og kreditt fra
pengeholdende sektor.

.. raw:: html

   <?php full_graph('makro-kilder.csv', 0, 'out', 2017); ?>

Grafen over viser en slik inndeling. Nederste linje **K2-uviss**,
omlag 20% i forhold til pengemengden er:
'Pensjonskasser', 'Obligasjonsgjeld', 'Sertifikatgjeld' og 'Andre kilder' i SSB-tabell 06718.
Disse vet jeg ikke om hører til pengeskapende sektor eller pengeholdende sektor.
Neste kurve **K2-kred** som i sin helhet er mindre enn 100% av pengemengden er kreditt fra pengeholdende sektor:
'Kredittforetak', 'Finansieringsselskaper', 'Livsforsikringsselskaper' og 'Skadeforsikringsselskaper'.
Siden disse ikke kan skape penger, må det som skjer her være at allerede eksisterende penger
blir re-allokert til andre kunder, altså at noen sparere investerer slik at de investerte pengene
kan lånes ut igjen.
Når **K2-kred** øker eller minker, så fører dette altså ikke til en tilsvarendre endring i pengemengden.
Det skulle derfor forbause meg veldig om denne kurven teoretisk sett kan overskride 100% av pengemengden.
Mellom 2008 og 2012 økte denne re-allokeringen voldsomt.
Var dette tilfeldig, eller var det et bevisst politisk valg om å fri-slippe kreditt- og finanisierings-foretak?
Er det skummelt eller helt OK at denne kredittmengden nå tilsvarer 90% av pengemengden?

**K2-bank** er kreditt som stammer fra pengeskapende og pengenøytral sektor og er summen av:
'Banker' og 'Statlige låneinstitutter' i SSB-tabell 06718.
Når **K2-bank** øker (minker) vil pengemengden øke (minke) med samme kronebeløp.
I utgangspunket skulle man trodd at K2-bank derfor alltid skulle være like stor som pengemengden, og
dermed flat 100% i dette diagrammet.
Grunnen til at denne kan kan avvike fra pengemengden er at pengeskapende sektor kan inndra
(og tilføre) penger
fra pengeholdende sektor ved å selge (og kjøpe) verdipapirer som kun misker (øker) pengemengden, ikke kredittmengden.
Er dette en fornuftig og innenfor hvilke rammer bør denne mekanismen brukes?
**K2-bank** var oppe i 160% av pengemengden i 2008 og har nå falt ned til 140% av pengemengden.
Det kan selvfølgelig tenkes at for stor pengemengde ville ha blitt
inflasjons-drivende i større grad enn man ønsker; det er jo velkjent at utlån fra private banker
medfører risiko for hyperinflasjon.
Man skulle likevel tro at innstramming i 'den andre enden', altså bankenes mulighet til å yte kreditt
i form av konsesjons-begrensninger eller liknende ville vært et nyttig verktøy som burde brukes
hyppigere for å unngå denne situasjonen. Eller, er dette ikke noe problem? Jeg vet ikke,
men lurer litt på om denne situasjonen er helt under kontroll.

Summen av alt dette er jo at kredittmengden nå er ca. 2,5 ganger så stor som pengemengden.
Den moderne måten å forklare penger på som jeg var inne på innledningsvis,
at banken skaper pengene ved utlån stemmer jo, men alle valgene som er tatt
(eller har blitt sånn), nemlig at det befinner seg kilder til kreditt også utenfor
pengeskapende sektor, og at vi har en skare av verdipapirer i form av obligasjoner o.l.
gjør bildet av banksektoren nokså uoversiktlig og kompleks.
Det store spørsmålet er: Er den for kompleks?

Kilder
^^^^^^

* `Steinar Holden, Makroøkonomi, 2016
  <https://www.cappelendamm.no/_fagboker/makro%C3%B8konomi-steinar-holden-9788202474096>`_
* `Erling Røed Larsen, Penger, 2012
  <https://www.ark.no/ark/catalog/productDetail.jsf?wec-appid=ARK&page=5C30D9D8686F4B559D643067CA3F0453&itemKey=0050569134641EE392EE265AA91929BA&wec-locale=no&gclid=CJ31sYj4vMACFeLTcgodfhAATA#.VALJG7vEh-g>`_
* `Marie Norum Lerbak, Om pengemengden, 2013
  <http://www.norges-bank.no/contentassets/8716a00c7c4f421fbd854400c4b534c2/staff_memo_2013_14.pdf>`_
* `Jon Nicolaisen, Hva skal våre penger være?, 2017
  <http://www.norges-bank.no/Publisert/Foredrag-og-taler/2017/2017-04-25-dnva/>`_
* `Statistikk fra SSB og andre kilder
  <regnskap_grunnlag>`_

.. raw:: html

   <hr>
   <a class="twitter-share-button" href="https://twitter.com/intent/tweet?text=Bankvesenet">Tweet</a>

   </div> <?php } include 'i.php'; ?>
